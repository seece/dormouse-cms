#!/usr/bin/perl
use warnings; use strict;
use File::Copy;

### get metadata for each page
sub get_meta {
    my $infile = shift;
    my $meta_name = shift;
    open INFILE, "<", $infile;
    my @file_contents = <INFILE>;
    close INFILE;

    ### data is stored in a html comment so it doesn't have to be removed
    foreach (@file_contents) {
        if (/<!--\s*?$meta_name=\s*?(.*?)\s*?-->/i) {
            return $1;
        }
    }

    if ($meta_name eq "title") {
        ### if there's no title, append the page name to the site title instead
        my $title = $infile;
        $title =~ s/\.html$//gi;
        return get_site_title() . " - " . $title;
    } elsif ($meta_name eq "description") {
        ### if there's no description, don't use one at all
        return "";
    } elsif ($meta_name eq "sort") {
        ### if there's no sort position, use 999999
        return "999999";
    } elsif ($meta_name eq "site-footer") {
        ### if there's no site-footer, use date
        (my $day, my $month, my $year) = (localtime)[3, 4, 5];
        return "last updated " . ($year + 1900) . "/" . ($month + 1) . "/" . "$day.";
    } elsif ($meta_name eq "site-title") {
        ### if there's no site-title, use something inoffensive. there should be a site-title though
        return "my website";
    }
}

### hopefully self-explanatory
sub get_site_footer { return get_meta("index.html", "site-footer"); }
sub get_site_title { return get_meta("index.html", "site-title"); }

### get the whole contents of a page
sub get_content {
    my $infile = shift;
    open INFILE, "<", $infile;
    my @file_contents = <INFILE>;
    close INFILE;

    my $content = "";
    foreach (@file_contents) {
        $content = $content . $_;
    }

    return $content;
}

### get the (optional) page header (always header.html)
sub get_header {
    if (-e "header.html") {
        return get_content("header.html");
    } else {
        return "";
    }
}

### create a menu for a directory of pages
sub generate_menu {
    my $menu = "<ul class=\"menu\">\n";
    my $dir = "."; 
    opendir(DIR, $dir);

    ### sort the menu by "sort" metadata
    my @array;
    while (my $file = readdir(DIR)) {
        push(@array, [get_meta($file, "sort"), $file]);
    }
    my @sorted_array = sort {
        $a->[0] cmp $b->[0];
    } @array;

    foreach (@sorted_array) {
        my $file = $_->[1];
        if ( ($file =~ /^.*?\.html$/i) && ($file !~ /^header\.html$/i) ) {
            my $title = get_meta($file, "title");
            $menu = $menu . "<li class=\"menu-item\"><a href=\"$file\" class=\"menu-link\">$title</a></li>\n";
        }
    }

    $menu = $menu . "</ul>";
    return $menu;
    closedir DIR;
}

### write a single page
sub write_page {
    (my $outfile, my $title, my $description, my $menu, my $header, my $content, my $footer) = @_;
    open OUTFILE, ">", $outfile;
    select OUTFILE;
    print "<!doctype html>\n";
    print "<html>\n";
    print "<head>\n";
    print "<meta charset=\"utf-8\" />\n";
    print "<title>$title</title>\n";
    print "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />\n";
    print "<meta name=\"description\" content=\"$description\" />\n";
    print "</head>\n";
    print "<body>\n";
    print "<table class=\"main\"><tr>\n";
    print "<td class=\"menu\">\n";
    print "$menu\n";
    print "</td><td class=\"body\">\n";
    if ($header ne "") {
        print "<div class=\"header\">\n";
        print "$header\n";
        print "</div>\n";
        print "<br />\n";
    }
    print "$content\n";
    print "</td>\n";
    print "</table><br />\n";
    print "<div class=\"footer\">\n";
    print "$footer\n";
    print "</div>\n";
    print "</body>\n";
    print "</html>\n";
    print "<!-- page created with dormouse-cms by aanaaanaaanaaana.net -->\n";
    close OUTFILE;
}

### recursively copy directories
### (only html and style.css will be copied in the top level
### but directories other than dormouse-output will be copied untouched
### so that you can use them to store images, etc)
sub rec_copy_dirs {
    my $dir = shift;
    if ($dir eq ".") {
        my @files = glob "*";
        foreach(@files) {
            my $file = $_;
            if ( ($file ne "dormouse-output") && (-d $file) ) {
                mkdir("dormouse-output/$file");
                rec_copy_dirs($file);
            }
        }
    } else {
        my @files = glob "$dir/*"; 
        foreach (@files) {
            my $file = $_;
            if (-f $file) {
                copy($file, "dormouse-output/$file");
            } elsif (-d $file) {
                mkdir("dormouse-output/$file");
                rec_copy_dirs($file);
            }
        }
    }
}

### build the whole site
sub build_site {
    my $footer = get_site_footer();
    my $menu = generate_menu();

    my $dir = "."; 
    opendir(DIR, $dir);
    mkdir("dormouse-output");

    ### copy style.css and foldered files
    copy("style.css", "dormouse-output/style.css");
    rec_copy_dirs(".");

    ### write html files to /dormouse-output/
    while (my $file = readdir(DIR)) {
        if ( ($file =~ /^.*?\.html$/i) && ($file !~ /^header\.html$/i) ) {
            write_page("dormouse-output/".$file, get_meta($file, "title"), get_meta($file, "description"), $menu, get_header(), get_content($file), $footer);
        }
    }

    closedir DIR;
}

build_site();
